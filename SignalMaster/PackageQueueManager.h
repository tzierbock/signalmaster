//
// File			PackageQueueManager.h
// Class library header
//
// Details		<#details#>
//
// Project	 	SignalMaster
// Developed with [embedXcode](http://embedXcode.weebly.com)
//
// Author		Tom Zierbock
// 				Tom Zierbock
//
// Date			21/08/2013 15:13
// Version		<#version#>
//
// Copyright	© Tom Zierbock, 2013
// Licence	    <#licence#>
//
// See			ReadMe.txt for references
//


// Core library - IDE-based
#if defined(MPIDE) // chipKIT specific
#include "WProgram.h"
#elif defined(DIGISPARK) // Digispark specific
#include "Arduino.h"
#elif defined(ENERGIA) // LaunchPad, FraunchPad and StellarPad specific
#include "Energia.h"
#elif defined(MAPLE_IDE) // Maple specific
#include "WProgram.h"
#elif defined(CORE_TEENSY) // Teensy specific
#include "WProgram.h"
#elif defined(WIRING) // Wiring specific
#include "Wiring.h"
#elif defined(ARDUINO) && (ARDUINO >= 100) // Arduino 1.0x and 1.5x specific
#include "Arduino.h"
#elif defined(ARDUINO) && (ARDUINO < 100)  // Arduino 23 specific
#include "WProgram.h"
#endif // end IDE

#ifndef PackageQueueManager_h

#define PackageQueueManager_h


class PackageQueueManager {
  
public:
  PackageQueueManager();
  void begin();
  String WhoAmI();
  void set(uint8_t data);
  uint8_t get();
  
private:
  uint8_t _data;
};

#endif
