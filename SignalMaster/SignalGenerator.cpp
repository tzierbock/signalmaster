//
// SignalGenerator.cpp 
// Class library C++ code
// ----------------------------------
// Developed with embedXcode 
// http://embedXcode.weebly.com
//
// Project 		SignalMaster
//
// Created by 	Tom Zierbock, 21/08/2013 12:17
// 				Tom Zierbock
//
// Copyright 	© Tom Zierbock, 2013
// Licence      Creative Commons
//
// See 			SignalGenerator.h and ReadMe.txt for references
//


// Library header
#include "SignalGenerator.h"

SignalGenerator::SignalGenerator() {
    
}

void SignalGenerator::begin() {
    initTimerCounter();
    startTimerCounter();
}

//  set up the control registers for the timer counter
void SignalGenerator::initTimerCounter() {
//#if defined(__AVR_ATmega2560__)
    
    //  set the OC pin as an output
    pinMode(5, OUTPUT);
    
    //  reset all the bits in the control registers 
    TCCR3A = 0;
    TCCR3B = 0;
    TCCR3C = 0;
    
    //  set timer 3 to CTC mode
    TCCR3B |= (1 << WGM32);
    //  toogle OC pin on output compare match
    TCCR3A |= (1 << COM3A0);
    //  set prescaler to 8
    TCCR3B |= (1 << CS31);
    //TCCR3B |= (1 << CS30);
    
//#endif
}

void SignalGenerator::startTimerCounter(){
//#if defined(__AVR_ATmega2560__)
    
    //  set the output compare value
    
    // a "1"
    //OCR3A = 113;
    
    // a "0"
    OCR3A = 226;
    
    //  set timer 3 to 0 (restart counting)
    TCNT3 = 0;
    
//#endif
}