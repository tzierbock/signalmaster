//
// File			SignalGenerator.h
// Class library header
//
// Details		DCC generation object for actualy creating the signal
//              on the output pin
//
// Project	 	SignalMaster
// Developed with [embedXcode](http://embedXcode.weebly.com)
//
// Author		Tom Zierbock
// 				Tom Zierbock
//
// Date			21/08/2013 12:17
// Version		0.1
//
// Copyright	© Tom Zierbock, 2013
// Licence	    Creative Commons
//
// See			ReadMe.txt for references
//


// Core library - IDE-based
#if defined(MPIDE) // chipKIT specific
#include "WProgram.h"
#elif defined(DIGISPARK) // Digispark specific
#include "Arduino.h"
#elif defined(ENERGIA) // LaunchPad, FraunchPad and StellarPad specific
#include "Energia.h"
#elif defined(MAPLE_IDE) // Maple specific
#include "WProgram.h"
#elif defined(CORE_TEENSY) // Teensy specific
#include "WProgram.h"
#elif defined(WIRING) // Wiring specific
#include "Wiring.h"
#elif defined(ARDUINO) && (ARDUINO >= 100) // Arduino 1.0x and 1.5x specific
#include "Arduino.h"
#elif defined(ARDUINO) && (ARDUINO < 100)  // Arduino 23 specific
#include "WProgram.h"
#endif // end IDE

#ifndef SignalGenerator_h

#define SignalGenerator_h


class SignalGenerator {
  
public:
    //  Basics
    SignalGenerator();
    void begin();
    
    //  Transmission Queue control functions
    bool setQueue(/**/);
    bool addPackageToQueue(/**/);
  
    //  Priority and E-Stop
    void setPriorityPackage(/**/);
    void eStop();   //send e-stop package to all trains (0x0) and remove signal from track
    void restart(); //continue transmitting packages
    
private:
    
    //  Timer counter control functions
    void initTimerCounter();
    void startTimerCounter();
    void stopTimerCounter();
    
};

#endif
