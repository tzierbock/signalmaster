// 
// SignalMaster 
//
// DCC Signal Generator
// Developed with [embedXcode](http://embedXcode.weebly.com)
// 
// Author	 	Tom Zierbock
// 				Tom Zierbock
//
// Date			21/08/2013 11:42
// Version		<#version#>
// 
// Copyright	© Tom Zierbock, 2013
// License		<#license#>
//
// See			ReadMe.txt for references
//

// Core library for code-sense
#if defined(WIRING) // Wiring specific
#include "Wiring.h"
#elif defined(MAPLE_IDE) // Maple specific
#include "WProgram.h"   
#elif defined(MPIDE) // chipKIT specific
#include "WProgram.h"
#elif defined(DIGISPARK) // Digispark specific
#include "Arduino.h"
#elif defined(ENERGIA) // LaunchPad, FraunchPad and StellarPad specific
#include "Energia.h"
#elif defined(CORE_TEENSY) // Teensy specific
#include "WProgram.h"
#elif defined(ARDUINO) && (ARDUINO >= 100) // Arduino 1.0 and 1.5 specific
#include "Arduino.h"
#elif defined(ARDUINO) && (ARDUINO < 100) // Arduino 23 specific
#include "WProgram.h"
#else // error
#error Platform not defined
#endif

// Include application, user and local libraries
#include "SignalGenerator.h"

// Define variables and constants
//
SignalGenerator generator;


//
// Brief:	Setup
//
// Add setup code 
void setup() {
    pinMode(13, OUTPUT);
    delay(100);
    digitalWrite(13, HIGH);
    
    delay(100);
    generator.begin();
}

//
// Brief:	Loop
//
// Add loop code 
void loop() {
  //    nothing
}
